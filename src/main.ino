#include "CA_Cert.h"
#include "fft.h"
#include "imageData.h"
#include "influx.h" //influx details (url/org/bucket/token)
#include "wifi.h" //Wifi credentials
#include <Arduino.h>
#include <HTTPClient.h>
#include <InfluxDbClient.h>
#include <InfluxDbCloud.h>
#include <M5StickC.h>
#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <WiFiMulti.h>
#include <driver/i2s.h>
#include <math.h>

// https://github.com/espressif/arduino-esp32/blob/master/tools/sdk/include/newlib/math.h
#define fpclassify(__x) ((sizeof(__x) == sizeof(float)) ? __fpclassifyf(__x) : __fpclassifyd(__x))

#ifndef isfinite
#define isfinite(__y) (__extension__({int __cy = fpclassify(__y); __cy != FP_INFINITE && __cy != FP_NAN; }))
#endif

#define DEVICE "ESP32"
#define PIN_CLK 0
#define PIN_DATA 34
#define SPL_MIN 52

#define MAX_BATCH_SIZE 5
#define WRITE_BUFFER_SIZE 30
#define WRITE_PRECISION WritePrecision::S

#define TZ_INFO "PST8PDT"
// For the fastest time sync find NTP servers in your area: https://www.pool.ntp.org/zone/
#define NTP_SERVER1 "time.oregonstate.edu"
#define NTP_SERVER2 "pool.ntp.org"

extern const unsigned char ImageData[768];

SemaphoreHandle_t xSemaphore = NULL;
SemaphoreHandle_t start_dis = NULL;
SemaphoreHandle_t start_fft = NULL;

WiFiMulti wifiMulti;

InfluxDBClient client(INFLUXDB_URL, INFLUXDB_ORG, INFLUXDB_BUCKET, INFLUXDB_TOKEN, InfluxDbCloud2CACert);
Point sensor("sound");
Point temperature("temperature");

int8_t i2s_readraw_buff[1024];
uint16_t posData = 160;
float splDb = 0;
long previousMillis = 0;
long interval = 1000;
// Number for loops to sync time using NTP
int iterations = 0;
float temp = 0; // temperature

TaskHandle_t xhandle_display = NULL;
TaskHandle_t xhandle_fft = NULL;

TFT_eSprite Disbuff = TFT_eSprite(&M5.Lcd);

void MicRecordfft(void *arg) {
  int16_t *buffptr;
  size_t bytesread;

  while (1) {
    xSemaphoreTake(start_fft, portMAX_DELAY);
    xSemaphoreGive(start_fft);
    i2s_read(I2S_NUM_0, (char *)i2s_readraw_buff, 1024, &bytesread, (100 / portTICK_RATE_MS));
    buffptr = (int16_t *)i2s_readraw_buff;

    splDb = soundPressureLevel(buffptr, bytesread / sizeof(uint16_t));
  }
}

void Drawdisplay(void *arg) {
  //uint16_t count_x = 0, count_y = 0;
  //uint16_t colorPos;
  float drawSpl = max(splDb, (float)SPL_MIN); // prevent zero value
  float maxSpl = max(splDb, (float)SPL_MIN); // prevent zero value

  while (1) {
    unsigned long currentMillis = millis();

    xSemaphoreTake(start_dis, portMAX_DELAY);
    xSemaphoreTake(xSemaphore, 500 / portTICK_RATE_MS);

    maxSpl = max(splDb, maxSpl);

    xSemaphoreGive(xSemaphore);
    xSemaphoreGive(start_dis);

    // Update the value only every `internval`
    if (currentMillis - previousMillis > interval) {
      previousMillis = currentMillis;
      drawSpl = maxSpl;
      maxSpl = max(splDb, (float)SPL_MIN); // Reset
      reportToInflux(drawSpl);
    }

    Disbuff.fillRect(0, 0, M5.Lcd.width(), M5.Lcd.height(), Disbuff.color565(0, 0, 0));
    Disbuff.setTextColor(WHITE);
    Disbuff.setTextSize(4);
    Disbuff.drawFloat(drawSpl, 0, 5, 5);
    Disbuff.pushSprite(0, 0);
  }
}

void setup() {
  M5.begin(true, true, true);
  M5.IMU.Init();
  M5.update();

  M5.Lcd.setRotation(1);
  M5.Lcd.setTextSize(2);

  // Connect WiFi
  M5.Lcd.println("Connecting to WiFi");
  Serial.println("Connecting to WiFi");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    M5.Lcd.print(".");
    Serial.print(".");
    delay(100);
  }
  M5.Lcd.println(" CONNECTED");
  Serial.println(" CONNECTED");

  timeSync(TZ_INFO, NTP_SERVER1, NTP_SERVER2);
  configTzTime(TZ_INFO, NTP_SERVER1, NTP_SERVER2);

  // Add constant tags - only once
  sensor.addTag("chipset", DEVICE);
  sensor.addTag("device", "M5Stick-C");

  temperature.addTag("chipset", DEVICE);
  temperature.addTag("device", "M5Stick-C");

  // Check server connection
  if (client.validateConnection()) {
    Serial.print("Connected to InfluxDB: ");
    Serial.println(client.getServerUrl());
  } else {
    Serial.print("InfluxDB connection failed: ");
    Serial.println(client.getLastErrorMessage());
  }

  client.setWriteOptions(WriteOptions().writePrecision(WRITE_PRECISION).batchSize(MAX_BATCH_SIZE).bufferSize(WRITE_BUFFER_SIZE));

  M5.Lcd.setSwapBytes(false);
  Disbuff.createSprite(M5.Lcd.width(), M5.Lcd.height());
  Disbuff.setSwapBytes(true);

  if (InitI2SMicroPhone() != true) {
    M5.Lcd.println("Mic error");
  }
  Disbuff.pushSprite(0, 0);

  xSemaphore = xSemaphoreCreateMutex();
  start_dis = xSemaphoreCreateMutex();
  start_fft = xSemaphoreCreateMutex();

  xSemaphoreTake(start_dis, portMAX_DELAY);
  xSemaphoreTake(start_fft, portMAX_DELAY);

  xTaskCreate(Drawdisplay, "Drawdisplay", 1024 * 8, (void *)0, 4, &xhandle_display);
  xTaskCreate(MicRecordfft, "MicRecordfft", 1024 * 2, (void *)0, 5, &xhandle_fft);
}

void loop() {
  delay(100);

/*
  // Sync time for batching once per hour
  if (iterations++ >= 360) {
    timeSync(TZ_INFO, NTP_SERVER1, NTP_SERVER2);
    iterations = 0;
  }
*/

  DisplayMicro();
}

bool InitI2SMicroPhone() {
  esp_err_t err = ESP_OK;
  i2s_config_t i2s_config = {
      .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX | I2S_MODE_PDM),
      .sample_rate = 44100,
      .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT, // is fixed at 12bit, stereo, MSB
      .channel_format = I2S_CHANNEL_FMT_ALL_RIGHT,
      .communication_format = I2S_COMM_FORMAT_I2S,
      .intr_alloc_flags = ESP_INTR_FLAG_LEVEL1,
      .dma_buf_count = 2,
      .dma_buf_len = 128,
  };

  i2s_pin_config_t pin_config;
  pin_config.bck_io_num = I2S_PIN_NO_CHANGE;
  pin_config.ws_io_num = PIN_CLK;
  pin_config.data_out_num = I2S_PIN_NO_CHANGE;
  pin_config.data_in_num = PIN_DATA;

  err += i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL);
  err += i2s_set_pin(I2S_NUM_0, &pin_config);
  err += i2s_set_clk(I2S_NUM_0, 44100, I2S_BITS_PER_SAMPLE_16BIT, I2S_CHANNEL_MONO);

  return (err == ESP_OK);
}

void DisplayMicro() {
  Disbuff.fillRect(0, 0, M5.Lcd.width(), M5.Lcd.height(), Disbuff.color565(0, 0, 0));
  Disbuff.pushSprite(0, 0);

  xSemaphoreGive(start_dis);
  xSemaphoreGive(start_fft);
  while ((!M5.BtnA.isPressed()) && (!M5.BtnB.isPressed())) {
    xSemaphoreGive(start_dis);
    xSemaphoreGive(start_fft);
    M5.update();
    // delay(100);
    xSemaphoreTake(start_dis, portMAX_DELAY);
    xSemaphoreTake(start_fft, portMAX_DELAY);
  }
  //xSemaphoreTake(start_dis, portMAX_DELAY);
  //xSemaphoreTake(start_fft, portMAX_DELAY);

  while ((M5.BtnA.isPressed()) || (M5.BtnB.isPressed())) {
    M5.update();
    delay(10);
  }
}

float soundPressureLevel(int16_t *data, uint16_t len) {
  double gain = 0.130; // Derived by comparing to real sound meter values
  int16_t *ptr;

  int16_t *end = data + len;
  double pref = 0.00002;

  /*******************************
   *   REMOVE DC OFFSET
   ******************************/
  int32_t avg = 0;
  ptr = data;
  while (ptr < end)
    avg += *ptr++;
  avg = avg / len;

  ptr = data;
  while (ptr < end)
    *ptr++ -= avg;

  /*******************************
   *   GET MAX VALUE
   ******************************/

  int16_t maxVal = 0;
  ptr = data;
  while (ptr < end) {
    int32_t v = abs(*ptr++);
    if (v > maxVal)
      maxVal = v;
  }

  double conv = ((float)maxVal) / 1023 * gain;

  /*******************************
   *   CALCULATE SPL
   ******************************/
  conv = 20 * log10(conv / pref);

  if (isfinite(conv))
    return conv;
  else
    return SPL_MIN;
}

void reportToInflux(float spl) {
  sensor.clearFields();
  sensor.setTime(time(nullptr));
  sensor.addField("db", spl);

  // Print what are we exactly writing
  Serial.print("Writing: ");
  Serial.println(client.pointToLineProtocol(sensor));

  // If no Wifi signal, try to reconnect it
  if ((WiFi.RSSI() == 0) && (wifiMulti.run() != WL_CONNECTED))
    Serial.println("Wifi connection lost");

  // Write point
  if (!client.writePoint(sensor)) {
    Serial.print("InfluxDB write sensor failed: ");
    Serial.println(client.getLastErrorMessage());
  }

  temperature.clearFields();
  temperature.setTime(time(nullptr));

  M5.IMU.getTempData(&temp);
  if (temp > 0) {
    // TODO: Compensate
    temperature.addField("celcius", temp);
    temperature.addField("fahrenheit", (temp * 9/5) + 32);
    Serial.println(client.pointToLineProtocol(temperature));
    if (!client.writePoint(temperature)) {
      Serial.print("InfluxDB write temp failed: ");
      Serial.println(client.getLastErrorMessage());
    }
  }

  // End of the iteration - force write of all the values into InfluxDB as single transaction
  Serial.println("Flushing data into InfluxDB");
  if (!client.flushBuffer()) {
    Serial.print("InfluxDB flush failed: ");
    Serial.println(client.getLastErrorMessage());
    Serial.print("Full buffer: ");
    Serial.println(client.isBufferFull() ? "Yes" : "No");
  }
}
